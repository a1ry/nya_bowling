import React, {Component} from 'react';
import {connect} from 'react-redux';
import { withRouter} from 'react-router';
import {Link} from 'react-router-dom';

class Profile extends Component {
  render() {
    const {
      firstName,
      lastName
    } = this.props.userData;
    return (
        <div className="h-full flex-col justify-center items-center px-64 pt-32">
          <div className="bg-blue-100 shadow-md rounded px-8 pt-6 pb-8">
            <div className="text-4xl mb-12">
              ¡Bienvenido, {`${firstName} ${lastName} !`}
            </div>
            <div className="flex flex-row w-full justify-center items-center">
              <Link
                  to="/record-game"
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-8 rounded-full mr-12">
                ¡Registrar juego!
              </Link>
              {/*<Link*/}
              {/*    to="/game-list"*/}
              {/*    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-8 rounded-full">*/}
              {/*  Ver mis juegos pasados*/}
              {/*</Link>*/}
            </div>
          </div>
        </div>
    );
  }
}

export default connect(
    ({ auth }) => {
      return {
        userData: auth.userData
      };
    },
    {}
)(withRouter(Profile));
