import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {doLogin} from '../apiActions/auth';
import {withRouter} from 'react-router';

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  login() {
    const { email, password } = this.state;
    if (!email || !password) {
      alert('Asegúrese de diligenciar los campos.');
    } else if (!emailRegex.test(email)) {
      alert('Asegúrese de ingresar un correo válido.');
    } else {
      this.props.doLogin(email, password)
          .then(() => {
            console.log('logging works!');
            this.props.history.push('/profile')
          })
          .catch((err) => {
            alert("Un error ha ocurrido, vuelve a intentar.");
          })
    }
  }

  render() {
    const { email, password } = this.state;
    return (
      <div className="h-full flex-col justify-center items-center px-64 pt-32">
        <p className="text-2xl text-center">Inicio de sesión</p>
        <form className="bg-white shadow-md rounded px-8 pt-6 pb-8">
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="username"
            >
              Correo electrónico
            </label>
            <input
              value={email}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="email"
              placeholder="Usuario"
              onChange={this.handleEmailChange}
            />
          </div>
          <div className="mb-6">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="password"
            >
              Contraseña
            </label>
            <input
              value={password}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              id="password"
              type="password"
              placeholder="******************"
              onChange={this.handlePasswordChange}
            />
          </div>
          <div className="flex items-center justify-center">
            <button
              className="mr-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="button"
              onClick={() => this.login()}
            >
              Entrar
            </button>
            <Link
              to="register"
              className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800"
            >
              Registrarme
            </Link>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(
    null,
    {
      doLogin
    }
)(withRouter(Login));
