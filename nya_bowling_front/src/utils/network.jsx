import Axios from 'axios';
import {apiPort, baseApiUrl} from '../config,jsx';

const axiosInstance = Axios.create({
  baseURL: `${baseApiUrl}:${apiPort}/`,
  headers: {
    'Content-Type': 'application/json'
    // 'Authorization': Config.authToken
  }
});

export const setAuthToken = authToken => {
  axiosInstance.defaults.headers['Authorization'] = `Bearer ${authToken}`;
};

export const get = url => {
  return axiosInstance.get(url);
};

export const post = (url, data) => {
  return axiosInstance.post(url, data);
};
